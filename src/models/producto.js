import mongoose from '../database/db'

const {Schema} = mongoose;

const objectId = Schema.ObjectId; 
const string = Schema.Types.String
const productSchema = new Schema({
    _id: {type: objectId},
    nombre: {type: string},
    categoria: {type: string},
    precio: {type: Number},
    user : {
        id: {type: objectId},
        nombre : {type: objectId}
    }
})

const product = mongoose.model('producto', productSchema);
export default product;