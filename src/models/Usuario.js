import { mongo, Mongoose, MongooseDocument } from 'mongoose';
import mongoose from '../database/db'
import bcrypt from 'bcryptjs'

const Schema = mongoose.Schema;
const string = Schema.Types.String;

const UserSchema = new Schema({
    username: {type: string, required: true},
     name: {type: string, required: true},
     email: {type: string, required: true},
     password: {type: string,required: true},
     role: {type: string,default: 'usuario', enum: 
   ['usuario','usuario_afiliado','admin']}
 });

 const Usuario = mongoose.model('Usuario', UserSchema);
 const encryptPassword = (password)=>{
        const salt = bcrypt.genSalt(10);
        return bcrypt.hash(password, salt);
 }
 const comparePassword = (input, password)=>{
    return bcrypt.compare(input, password)
 }

 export default {Usuario, encryptPassword, comparePassword};