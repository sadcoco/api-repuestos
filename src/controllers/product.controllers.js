import producto from '../models/producto';

export const crearProducto = async (req, res) => {
   const { nombre, categoria, precio, idUser, username} = req.body;
   const { id } = req.params
   try {
       const productoCreado =  await producto.create({nombre: nombre, categoria: categoria, 
        precio: precio, user:{id: idUser, nombre: username}})
        res.status(200).json({message: "Producto creado satisfactoriamente"})
        console.log(productoCreado)
   }catch(error){
       console.log(error)
   }
};
export const getProductos = (req, res) => {
    try {
        res.send(await producto.find({}))
    }catch(error){
        console.log(error)
    }
};

export const getProductoById = (req, res) => {
    const {id} = req.params
    try {
        let producto = await producto.findOne({_id:id})
        res.status(200).json({producto})
    }catch(error){
        console.log(error)
    }
};

export const borrarProducto = (req, res) => {
    let id = req.params
    try{
        let productoBorrado = await producto.remove({_id:id})
        res.status(200).json({message: "Producto borrado satisfactoriamente"})
        console.log(productoBorrado)
    }catch(error){
        console.log(error)
    }
};

export const updateProducto = (req, res) => {
    let  {id} = req.params
    let {nombre, precio}
    try {
        let actualizado = await producto.findByIdAndUpdate({_id:id}, {nombre: nombre, precio: precio})
        res.send("Producto actualizado exitosamente")
        console.log(actualizado)
    }catch(error){
        console.log(error)
    }
};
export const selectByCategory = async (req,res) => {
    let {categoria} = req.body
    try {
        let encontrados = await producto.find({categoria: categoria})
        res.send("productos encontrados " + encontrados)
    }catch (error){
        console.log(error)
    }
}
export const selectByUserName = async (req,res) => {
    let {username} = req.body
    try {
        let productos = await producto.find({user: {nombre: username}})
        res.status(200).send(productos)
    }catch(error){
        console.log(error)
    }
}
