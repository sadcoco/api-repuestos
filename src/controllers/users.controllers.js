import Usuario from '../models/Usuario';
import mongoose from 'mongoose'
let user = mongoose.model('Usuario')
const getUsers = async (req, res) => {
    try {
        const users = await Usuario.Usuario.find();
        res.status(200).json({ data: users });

    } catch (error) {
        console.log(error);

    }
};

const getUsersById = async (req, res) => {
    try {
        const { id } = req.params;
        const user = await Usuario.Usuario.findById({id});
        res.status(200).json({ data: user });

    } catch (error) {
        console.log(error);

    }
};


const updateUser = async (req, res) => {
    const { id } = req.params;
    const { username, name, password } = req.body;
try{
    let user = await Usuario.Usuario.findByIdAndUpdate({_id: id},{
        username:username,
        name: name, 
        password: await Usuario.encryptPassword(password)})

   
    return res.json({
        message: 'Actualizados los datos del usuario',
        data: user
    })
}catch(err){
    console.log(err)
}
};

const deleteUser = async (req, res) => {
    try {
        const { id } = req.params;
        const user = await Usuario.Usuario.deleteOne({
            id
        });
        res.status(200).json({ 
            message: 'Usuario eliminado satisfactoriamente',
            data: user 
    });
    } catch (error) {
        console.log(error);

    }
}

module.exports = { getUsers, getUsersById, deleteUser, updateUser };
