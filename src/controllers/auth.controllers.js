import Usuario from '../models/Usuario';
import jwt from 'jsonwebtoken'
import config from '../config'
import {encryptPassword, comparePassword} from '../models/Usuario'
import moment from 'moment'
export const signup = async (req, res) =>{
    const {name, email, password, role} = req.body
    const newUser = new Usuario({
        username,
        name,
        email,
        password: await Usuario.encryptPassword(password)
    })
    if (role){
       const foundRoles =  await role.findOne({role: {$in: Usuario}})
       newUser.roles = foundRoles.map(role => role._id)
    } else {
        const role = await role.findOne({role : 'usuario'})
        newUser.role = role
    }
    console.log(newUser)
    const saveUser = await newUser.save()
   const token = jwt.sign({id: saveUser.id}, config.secretKey,{
       expiresIn: 86400
   })
}
export const login = async (req, res) => {
    const userFound = await userFound.findOne({email: req.body.email}).populate("roles")
    if (!userFound) return res.status(400).json({message: 'User Not Found'})

    const matchPassword = await Usuario.comparePassword(req.body.password, userFound.password)
    if (! matchPassword()) return res.status(401).json({token: null, msg: 'invalid password'})

    let id = userFound._id
    let m = moment.unix()
    let payload = {
        sub: id,
        iat: m,
        exp: m.add(3, "hours").unix()
    }
    const token= jwt.sign(payload, config.secretKey, {expiresIn: 86400})
    res.json({token: token})
}
