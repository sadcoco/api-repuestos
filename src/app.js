import express, { json } from 'express';
import morgan from 'morgan';

//initialization
const app = express();

//middlewares

app.use(morgan('dev'));





import pkg from '../package.json';
import productsRoutes from './routes/products/products.routes';
import userRouter from './routes/user'
import imgRoutes from './routes/image.routes'
const bodyParser = require('body-parser');

app.set('pkg',pkg);

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
//app.use(AuthTok);


import apiAuthRouter from './routes/auth.routes'

app.use('/', apiAuthRouter)
app.use('/products',productsRoutes);
app.use('/user', userRouter)
app.use('/photo',imgRoutes)


export default app;