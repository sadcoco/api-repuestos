const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/RepuestosDB',{
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(()=> {console.log("Conexion exitosa")})
.catch(()=> {console.log("Error de conexión: no se pudo conectar a la base de datos")});
export default mongoose;