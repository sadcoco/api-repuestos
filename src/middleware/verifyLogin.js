import jwt from 'jsonwebtoken'
import config from '../config'

export const isLoggedIn =  (req,res,next) => {
    if (!req.headers.authorization){
        return res.status(403).json({message: "Para estar aquí debe inciar sesión"})
    }
    let token = req.headers.authorization.split(" ")[1]
    let payload = jwt.decode(token, config.secretKey)

    if (token.exp <= moment.unix()){
        return res.status(403).json({message: "Sus sesión ha expirado"})
    }
    next()
}