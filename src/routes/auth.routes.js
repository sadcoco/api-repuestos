import {Router} from 'express';

const router = Router();

import {signup} from '../controllers/auth.controllers'
import {login} from '../controllers/auth.controllers'

router.post('/signup',signup);
router.get('/login', login)
module.exports = router