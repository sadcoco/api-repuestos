import {Router} from 'express'
import upload from '../middleware/upload'
import fs from 'fs'

const router = Router()

router.post('/uploadphoto', upload.single('p'), (req,res,next) =>{
    let file  = req.file
    if (!file){
        const error = new Error('Escoja un archivo')
        error.httpStatusCode = 400
        return next(error)
    }
    res.send(file)
})
router.post('/uploadphotos',upload.array('p', 3), (req,res,next)=>{
    uploads(req,res, function(err){
        if(err){
            console.log(err)
            return res.end('Error intentando cargar el archivo')
        }
        res.redirect('/')
        
    })
})
export default router