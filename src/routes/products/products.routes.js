import {Router} from 'express';

const router = Router();

import {crearProducto, getProductos,updateProducto,getProductoById,borrarProducto} from '../../controllers/product.controllers';
router.post('/', crearProducto);
router.get('/', getProductos);
router.put('/:productId',  updateProducto);
router.get('/:productId', getProductoById);
router.delete('/:productId', borrarProducto);
export default router;