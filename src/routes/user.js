import {getUsers, getUsersById,updateUser, deleteUser} from '../controllers/users.controllers';
import {Router} from 'express';


const router = Router()


router.get('/SearchUsers',getUsers)
router.get('/:userid', getUsersById)
router.put('/profile', updateUser)
router.delete('/removeUser', deleteUser)

export default router